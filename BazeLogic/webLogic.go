package BazeLogic

import (
	"bufio"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
)

type Guestbook struct {
	SignatureCount int
	Signatures     []string
}

func Check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
func ViewHandler(w http.ResponseWriter, _ *http.Request) {
	signatures := GetStrings("signatures.txt")
	html, err := template.ParseFiles("view.html")
	Check(err)
	guestbook := Guestbook{
		SignatureCount: len(signatures),
		Signatures:     signatures,
	}
	err = html.Execute(w, guestbook)
	Check(err)
	file()
}
func CreateHandler(w http.ResponseWriter, r *http.Request) {
	signature := r.FormValue("signature")
	options := os.O_WRONLY | os.O_APPEND | os.O_CREATE
	file, err := os.OpenFile("signatures.txt", options, os.FileMode(0600))
	Check(err)
	_, err = fmt.Fprintln(file, signature)
	Check(err)
	http.Redirect(w, r, "/guestbook", http.StatusFound)
}
func GetStrings(fileName string) []string {
	var lines []string
	file, err := os.Open(fileName)
	if os.IsNotExist(err) {
		return nil
	}
	Check(err)
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	Check(scanner.Err())
	return lines
}
func NewHandler(w http.ResponseWriter, _ *http.Request) {
	html, err := template.ParseFiles("new.html")
	Check(err)
	err = html.Execute(w, nil)
	Check(err)
}
func BarHandler(w http.ResponseWriter, _ *http.Request) {
	html, err := template.ParseFiles("longe.html")
	Check(err)
	err = html.Execute(w, nil)
	Check(err)
}

func MissionHandler(w http.ResponseWriter, _ *http.Request) {
	html, err := template.ParseFiles("mission.html")
	Check(err)
	err = html.Execute(w, nil)
	Check(err)
}
func DirectionHandler(w http.ResponseWriter, _ *http.Request) {
	html, err := template.ParseFiles("directions.html")
	Check(err)
	err = html.Execute(w, nil)
	Check(err)
}
func EllxirHandler(w http.ResponseWriter, _ *http.Request) {
	html, err := template.ParseFiles("ellxir.html")
	Check(err)
	err = html.Execute(w, nil)
	Check(err)
}
func CaffeeHandler(w http.ResponseWriter, _ *http.Request) {
	html, err := template.ParseFiles("caffee.html")
	Check(err)
	err = html.Execute(w, nil)
	Check(err)
}
func CaffeePage(w http.ResponseWriter, _ *http.Request) {
	html, err := template.ParseFiles("caffeePage.html")
	Check(err)
	err = html.Execute(w, nil)
	Check(err)
}
func FreeCoffeeCupon(w http.ResponseWriter, _ *http.Request) {
	html, err := template.ParseFiles("freecoffee.html")
	Check(err)
	err = html.Execute(w, nil)
	Check(err)
}
func Ordering(w http.ResponseWriter, _ *http.Request) {
	html, err := template.ParseFiles("order.html")
	Check(err)
	err = html.Execute(w, nil)
	Check(err)

}
func Location(w http.ResponseWriter, _ *http.Request) {
	html, err := template.ParseFiles("locationMap.html")
	Check(err)
	err = html.Execute(w, nil)
	Check(err)
}
func file() {
	_, err := os.Stat("signature.txt")
	if errors.Is(err, os.ErrNotExist) {
		fmt.Println("fillle ")
	}
}
