package main

import (
	p "Web/BazeLogic"
	"log"
	"net/http"
)

func main() {
	webShowSite()
}

func webShowSite() {

	fs := http.FileServer(http.Dir("assets"))
	http.Handle("/assets/", http.StripPrefix("/assets", fs))
	http.HandleFunc("/caffee", p.CaffeeHandler)
	http.HandleFunc("/freecoffee", p.FreeCoffeeCupon)
	http.HandleFunc("/caffeePage", p.CaffeePage)
	http.HandleFunc("/ellxir", p.EllxirHandler)
	http.HandleFunc("/mission", p.MissionHandler)
	http.HandleFunc("/guestbook", p.ViewHandler)
	http.HandleFunc("/longe", p.BarHandler)
	http.HandleFunc("/directions", p.DirectionHandler)
	http.HandleFunc("/guestbook/new", p.NewHandler)
	http.HandleFunc("/new", p.NewHandler)
	http.HandleFunc("/guestbook/create", p.CreateHandler)
	http.HandleFunc("/order", p.Ordering)
	http.HandleFunc("/locationMap", p.Location)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)

}
